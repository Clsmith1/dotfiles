vim.g.mapleader = " "
local key = vim.keymap


-- 1st element is MODE, 2nd is REMAP, 3rd is BINDING


key.set("i", "ei", "<ESC>", { desc = "exit insert mode with ei" })

key.set("n", ";", ":", { desc = "easy way to get into cmd mode" })


-- spit pane vertically
key.set("n", "<Leader>|", "<cmd>vsp<cr>")
