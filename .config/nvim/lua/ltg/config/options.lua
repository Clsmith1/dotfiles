local opt = vim.opt

-- line numbers
opt.relativenumber = true
opt.number = true

-- disable line wrapping
opt.wrap = false

-- search stuff	
opt.ignorecase = true
opt.smartcase = true


-- cursor line
opt.cursorline = true

-- tabs & identation
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

-- UI stuff
opt.termguicolors = true
opt.signcolumn = "yes"
opt.background = "dark"

-- fix backspace
opt.backspace = "indent,eol,start"

opt.clipboard:append("unnamedplus")

-- split windows
opt.splitright = true
opt.splitbelow = true

-- turn off swapfile
opt.swapfile = false




