return {
  "nvim-treesitter/nvim-treesitter",
  event = { "BufReadPre", "BufNewFile" },
  build = ":TSUpdate",
  dependencies = {
    "JoosepAlviste/nvim-ts-context-commentstring",
    "windwp/nvim-ts-autotag",
    "hiphish/rainbow-delimiters.nvim",
    "windwp/nvim-autopairs",
  },

  config = function()
    local treesitter = require("nvim-treesitter.configs")


    -- configure ts
    treesitter.setup({
      highlight = {
        enable = true,
      },

      indent = { enable = true },

      autotag = { enable = true },


      ensured_installed = {
        "yaml",
        "lua",
        "bash",
        "rust",
        "vim",
        "c",
        "cpp",
        "python",
        "toml",
      },

      highlight = {
        enable = true,
        additonal_vim_regex_highlighting = false,
        custom_captures = {
          ["punctuation.bracket"] = "",
          ["constructor"] = "",
        },
      },

      autotag = {
        enable = true,
      },

      rainbow = {
        enable = true,
      },

      autopairs = {
        enable = true,
      },


      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
    })
  end,
}
