return {
  "folke/trouble.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons", "folke/todo-comments.nvim" },
  keys = {
    { "<leader>xx", "<cmd>TroubleToggle<CR>",                       desc = "Open/Close trouble list" },
    { "<leader>xw", "<cmd>TroubleToggle workspace_diagnostics<CR>", desc = "Open Trouble workspace" },
    { "<leader>xq", "<cmd>TroubleToggle quickfix<CR>",              desc = "open trouble quickfix" },
    { "<leader>xt", "<cmd>TodoTrouble<CR>",                         desc = "Open TODO in trouble" },

  },
}
