-- XMonad --
import XMonad
import XMonad.Operations
import System.IO
import System.Exit (exitSuccess)

-- Data --
import Data.Monoid
import Data.Maybe(isJust)

-- Actions --
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)

-- Hooks --
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.WindowSwallowing

-- Layouts --
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.LayoutModifier
import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.MultiToggle as MT (Toggle(..))

-- Utilities --
import XMonad.Util.SpawnOnce
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP, mkKeymap)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Cursor
-- Qualified -- 
import qualified Data.Map as M
import qualified XMonad.StackSet as W
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))

myTerminal :: String
myTerminal = "kitty"
myModMask  = mod4Mask

myNormalColor :: String
myNormalColor = "#74c7ec"

myFocusColor :: String
myFocusColor = "#cba6f7"

myBorderWidth :: Dimension
myBorderWidth = 2
myWorkspaces :: [String]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myStartupHook :: X ()
myStartupHook = do
             spawnOnce "nitrogen --restore"
             spawnOnce "/usr/bin/picom --vsync"
             spawn "emacs --daemon"
             spawnOnce "$HOME/.local/bin/monitor_setup.sh"
             spawn "gentoo-pipewire-launcher"
             spawn "sleep 2 && trayer-srg --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 --tint 0x292d3e --height 18 &"
             setDefaultCursor xC_left_ptr
             setWMName "LG3D"

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True  (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ mySpacing 6
           $ ResizableTall 1 (3/100) (1/2) []

monocle  = renamed [Replace "monocle"]
           $ limitWindows 20 Full

floats   = renamed [Replace "floats"]
           $ limitWindows 20 simplestFloat

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
      where fadeAmount = 1.0

myLayoutHook = avoidStruts $ windowArrange $ T.toggleLayouts floats $
               mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
              where
              myDefaultLayout =  tall
                              ||| noBorders monocle
                              ||| floats

myScratchPads = [
-- run htop in xterm, find it by title, use default floating window placement
    NS "htop" "st -e htop" (title =? "htop") defaultFloating ,

-- run stardict, find it by class name, place it in the floating window
-- 1/6 of screen width from the left, 1/6 of screen height
-- from the top, 2/3 of screen width by 2/3 of screen height
    NS "stardict" "stardict" (className =? "Stardict")
        (customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)) ,

-- run gvim, find by role, don't float
    NS "notes" "gvim --role notes ~/notes.txt" (role =? "notes") nonFloating
                ]
                 where role = stringProperty "WM_WINDOW_ROLE"

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)

myManageHook = composeAll
     
  [ className =? "confirm"         --> doFloat
  , className =? "file_progress"   --> doFloat
  , className =? "dialog"          --> doFloat
  , className =? "download"        --> doFloat
  , className =? "error"           --> doFloat
  , className =? "Gimp"            --> doFloat
  , className =? "notification"    --> doFloat
  , className =? "pinentry-gtk-2"  --> doFloat
  , className =? "splash"          --> doFloat
  , className =? "toolbar"         --> doFloat
  , className =? "Yad"             --> doCenterFloat
  , title =? "Oracle VM VirtualBox Manager"   --> doFloat
  , title =? "Order Chain - Market Snapshots" --> doFloat
  , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
  , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 1 )
  , className =? "mpv"             --> doShift ( myWorkspaces !! 7 )
  , className =? "Gimp"            --> doShift ( myWorkspaces !! 8 )
  , className =? "VirtualBox Manager" --> doShift  ( myWorkspaces !! 4 )
  , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
  , isFullscreen -->  doFullFloat
  ]

myKeys :: [(String, X ())]
myKeys = 
       -- Applications --
    [ ("M-<Return>", spawn myTerminal), -- open myTerminal (usually Kitty or Alacritty)
      ("M-p", spawn "dmenu_run"), -- open dmenu prompt
      ("M-s", spawn "flameshot gui"), -- open flameshot (screen shot utility)
      
      -- Browsers --
      ("M-b f", spawn "firefox-bin"), -- open firefox browser, remember to switch to firefox if you didn`t compile the binary package
      ("M-b b", spawn "brave"), -- open brave web browser
      ("M-b q", spawn "qutebrowser"), -- open qutebrowser

      -- Scratchpads --
      ("M-t", namedScratchpadAction myScratchPads "htop"), -- open htop scratch pad
      ("M-S-t",  namedScratchpadAction myScratchPads "mocp"),

      -- Emacs --
      ("M-e e", spawn "emacsclient -c -a 'emacs'"), -- open up emacsclient (make sure to have emacs daemon start in autostart for this to work)
      ("M-e d", spawn "emacsclient -c -a '' --eval '(dired nil)'"), -- open up emacs 'dired (the emacs file manager)

      -- Layouts --
      ("M-<Tab>", sendMessage NextLayout), -- switch layouts
      ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts), -- toggle struts on/off, this will allow windows to overlap the xmobar
      ("M-f", sendMessage (T.Toggle "floats")), -- set the window to float
      ("M-<Delete>", withFocused $ windows . W.sink), -- push floating window back to tile
      ("M-S-<Delete>", sinkAll), -- push ALL floating windows back to tile
      
      -- Window Navigation 
      ("M-m", windows W.focusMaster), -- move focus to the master window
      ("M-j", windows W.focusDown), -- move focus to the next window
      ("M-k", windows W.focusUp), -- move focus to the prev window
      ("M-S-j", windows W.swapDown), -- swap focused window with next window
      ("M-S-k", windows W.swapUp), -- swap focused window with prev window
      ("M-S-<Return>", promote),   -- move focused window to master, others stay the same
      ("M1-S-<Tab>", rotSlavesDown), -- Rotate all windows except master and keep focus
      ("M1-C-<Tab>", rotAllDown), -- Rotate all the windows in the current stack
      ("C-s", killAllOtherCopies), -- kill the other windows
      
      -- Media Keys --
      ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -1.5%"), -- allow keyboard media keys to turn down the volume, use the program xev to find out what your media key is 
      ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +1.5%"), -- allow keyboard media keys to turn up the volume, again use xev to find out your media key

      -- Xmonad --
      ("M-S-c", kill1), -- kill the current focused window
      ("M-S-a", killAll), -- killall windows on current workspace
      ("M-S-r", spawn "xmonad --restart"), -- restart xmonad
      ("M-q", io exitSuccess) -- exit xmonad
    ]
    
        where nonNSP = WSIs (return (\ws -> W.tag ws /= "nsp"))
              nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

main :: IO ()
main = do
       -- launch Xmobar
       xmproc <-  spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobarrc")
       xmproc1 <- spawnPipe ("xmobar -x 1 ~/.config/xmobar/xmobarrc")
       xmproc2 <- spawnPipe ("xmobar -x 2 ~/.config/xmobar/xmobarrc2")

       xmonad $ docks $ ewmh $ def
--{ manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
         { manageHook = myManageHook <+> manageDocks <+> namedScratchpadManageHook myScratchPads,
          terminal        = myTerminal,
          modMask         = myModMask,
         -- keys            = myKeys,
          workspaces      = myWorkspaces,
          handleEventHook = swallowEventHook (className =? "Alacritty" <||> className =? "kitty" <||> className =? "St-256") (return True),
          layoutHook      = myLayoutHook,
          focusedBorderColor = myFocusColor,
          normalBorderColor = myNormalColor,
          borderWidth     = myBorderWidth,
          startupHook     = myStartupHook,
         -- logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
            logHook = dynamicLogWithPP $ filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x >> hPutStrLn xmproc1 x >> hPutStrLn xmproc2 x,
                          ppCurrent = xmobarColor "#fbe0dc" "" . wrap ("<box type=Bottom width=4 color=" ++ "#cba6f7" ++ ">") "</box>",
                          ppVisible = xmobarColor "#f38ba8" "",
                          ppHidden = xmobarColor "#a6e3a1" "" . wrap "*" "",
                          ppHiddenNoWindows = xmobarColor "#89b4f4" "",
                          ppTitle = xmobarColor "#cdd6f4" "" . shorten 60,
                          ppUrgent = xmobarColor "#fab387" "" . wrap "!" "!",
                          ppSep = "<fc=#313244> | </fc>"
                        }
             
         } `additionalKeysP` myKeys
