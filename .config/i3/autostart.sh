#!/usr/bin/env sh
~/.config/polybar/launch.sh &
gentoo-pipewire-launcher &
nitrogen --restore &
#picom --experimental-backends --backend glx --xrender-sync-fence --vsync &
/usr/local/bin/picom &
#emacs daemon
emacs --daemon &

~/.local/bin/monitor_setup.sh &
