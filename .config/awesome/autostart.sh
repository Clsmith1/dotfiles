#!/usr/bin/env bash


function run {
	if ! pgrep $1 ;
        then
	$@&
	fi
}

run picom --vsync &
run nitrogen --restore
run emacs --daemon &
