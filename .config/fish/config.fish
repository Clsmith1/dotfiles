set -g fish_prompt_pwd_dir_length 0
set -g fish_greeting ''

alias ls="$HOME/.cargo/bin/eza -lh --color always --group-directories-first --icons"
#alias ls="/usr/bin/lsd -lh --color always --group-directories-first"
alias cat="/usr/bin/bat"
alias bare="/usr/bin/git --git-dir=$HOME/.config/dotfiles.git --work-tree=$HOME"
if status is-interactive
    # Commands to run in interactive sessions can go here
end

# Enviroment variables for starship prompt error msgs
#export STARSHIP_CACHE=$HOME/.starship/cache/

set -Ua fish_user_paths $HOME/.cargo/bin/

neofetch
zoxide init fish | source 
starship init fish | source
